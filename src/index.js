import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Header from './components/header/Header';
import Footer from './components/footer/Footer';
import Tasks from './components/tasks/Tasks';
import Articles from './components/articles/Articles';
import Contact from './components/contact/Contact';
import Homepage from './components/homepage/Homepage';
import About from './components/about/About';
import * as serviceWorker from './serviceWorker';
import { Route, BrowserRouter as Router } from 'react-router-dom';

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from './reducers'


const store = createStore(reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

ReactDOM.render(
    <Router>
        <div>
            <Provider store={store}>
                <Header />
                    <Route exact path="/" component={Homepage} />
                    <Route path="/tasks" component={Tasks} />
                    <Route path="/articles" component={Articles} />
                    <Route path="/about" component={About} />
                    <Route path="/contact" component={Contact} />
                <Footer />
            </Provider>
        </div>
    </Router>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
