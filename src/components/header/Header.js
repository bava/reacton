import React from "react"
import './header.css';
import { Link } from 'react-router-dom'

import Clock from './clock/Clock';
import TemporaryDrawer from '../../material/Drawer/TemporaryDrawer';
import Logo2 from '../../assets/img/Logo2.png' // relative path to image 


function Header() {
    return (
        <header className="header">
            <nav className="header-nav">
                <div>
                    <img src={Logo2} className="header-nav_logo" alt="logo"/>   
                </div>

                <Clock />
                <TemporaryDrawer />
            </nav>
        </header>
    )
}

export default Header;
