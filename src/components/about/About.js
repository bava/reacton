import React from "react"
import { useEffect, useState } from 'react';
import './about.css';
import AdvancedGridList from '../../material/AdvancedGridList';
import 'react-clock/dist/Clock.css';

function About() {
    return (
        <section className="about">
            <h1>Our preferences</h1>
            <div className="about-main">
                <div>
                    <AdvancedGridList />
                </div>
            </div>
        </section>
    )
}

export default About