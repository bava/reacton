import React from "react"
import './articles.css';
import articlesList from './articleList.js';
import ControlledExpansionPanels from '../../material/Panel/ControlledExpansionPanels';

function Articles() {
    return (
        <section className="articles">
            <h1>Articles</h1>
            <div className="articles-main">
                {articlesList.map((article) => {
                    return (
                        <div key={article.id} className="articles-main_block">
                            <ControlledExpansionPanels   
                                title={article.title}
                                text={article.text}
                            />
                        </div>
                    )
                })}
            </div>
        </section>
    )
}

export default Articles