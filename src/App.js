import React, { Component } from 'react';
import './App.css';
import Header from './components/header/Header';
import Footer from './components/footer/Footer';
import Homepage from './components/homepage/Homepage';

class App extends Component {
  render() {
    return (
      <div className="wrap">
        <Header />
        <Homepage />
        <Footer />
      </div>
    );
  }
}

export default App;
