import React from 'react'

var divStyle = {
    textAlign: 'center',
    paddingTop: 100,
    paddingBottom: 100,

  };

const Notfound = () => {
    return (
        <h1 style={divStyle}>Not found</h1>
    )  

}

export default Notfound